function addPlayer() {
  const newPlayerName = document.querySelector('input[type="text"]').value;
  const requestBody = [
    {
      id: 0,
      name: newPlayerName,
    },
  ];
  fetch("http://127.0.0.1:8000/player/add", {
    method: "POST",
    body: JSON.stringify(requestBody),
    headers: { "Content-Type": "application/json" },
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to add new player");
      }
      return response.json();
    })
    .then((data) => {
      console.log(`Successfully added player with ID ${data.id}`);
      // do something with the response data
    })
    .catch((error) => {
      console.error(error);
      // handle the error
    });
}
