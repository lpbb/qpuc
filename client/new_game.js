// make HTTP request to retrieve player data from API
function getPlayers() {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "http://127.0.0.1:8000/", true);
  xhr.onload = function () {
    if (this.status === 200) {
      const players = JSON.parse(this.responseText);
      const playerList = document.querySelector("#players ul");
      players.forEach(function (player) {
        const li = document.createElement("li");
        li.innerHTML = player.name + " (" + player.id + ")";
        playerList.appendChild(li);
      });
    }
  };
  xhr.send();
}

// call getPlayers function to populate player list on page load
getPlayers();
