# QPUC

## Présentation

Ce projet fait office de rendu pour le projet de Génie Logiciel. QPUC ou 'Question Pour Un Cuck' est un jeu de quizz de culture générale crée par une bande d'amis. Une partie basique se compose de 4 niveaux de difficultés avec 5 questions pour chaque niveau de difficulté. Chaque question aborde un thème différent (musique, sport, jeux vidéos, science...). Il y a donc un total de 20 questions pour une partie simple, et donc 4 questions par thèmes différents. 

Chacun des joueurs démarre la partie avec 5 points de vie. Si un joueur réponds faux à une question, il perd une vie. Cependant, il est possible d'ajouter des questions spéciales à la partie, dites 'Question Sustain'. Les joueurs qui répondent juste à une question sustain regagnent un point de vie. 

Tant qu'il y a des joueurs encore en vie, la partie continue. S'il n'y a plus de questions, l'application continuera de piocher des questions bonus au hasard de difficulté maximale.

L'application veille bien à ce qu'aucun joueur de la partie n'ai déjà rencontré une des questions de la partie actuelle auparavant. (Que ce soit parmis les questions initiales de la partie ou parmis les questions bonus piochées au hasard.)

L'heureux gagnant de Question Pour Un Cuck se voit le plaisir de devenir le nouveau Maître Cuck : celui qui s'occupe de gérer la prochaine partie.

Les informations de chaque joueur, chaque question et chaque parties sont stockées dans une base de données. Ainsi, le maître cuck (l'utilisateur de l'application) a la possibilité d'ajouter de nouvelles questions ou de nouveaux joueurs à la base de données.

Pour l'instant l'application fonctionne uniquement localement. Elle est donc adaptée au meneur du quizz et c'est seulement lui qui peut utiliser cette dernière. Mais un système de connexion entre joueur est envisageable pour améliorer l'application.

## Quelques mots sur les attendues du projets

Plusieurs attendues étaient requis pour valider le projet, voici une explication pour chacun de ces attendues :

- Le design pattern : Le design pattern qui a été choisit est la Fabrique (Factory). C'est une classe qui permet de généraliser la création d'objet de différentes classes. Ici, nous avons une fabrique de questions, de joueurs et de parties.
- La base de données : Cette dernière devait être suffisament complexe. La notre est composée de 3 tables : une table joueur avec l'id des joueur et leur nom. Une table question, avec l'id, le contenue, la réponse, le thème et la difficulté de chaque questions. Et une table game, avec l'id de la partie et l'id du joueur vainquer de chaque partie. Si un jouer n'a pas gagné de partie alors l'id du vainqueur est de zéro. De plus, il y a deux tables d'associations : La table belongs_in, qui relie les id des questions aux id de game, ainsi nous pouvons savoir quelles questions ont été posés à chaque partie. Et la table played_in, qui relie les id des joueurs aux id de game, ainsi nous pouvons savoir quels joueurs ont joués à chaque partie. Ce qui fait un total de 5 tables.
- Les test : Chacune des méthodes de notre couche métiers ont été testés. Il en est de même pour les méthodes permettant de requêter la base de données.
- Lancer l'application avec Docker :

## Comment lancer le serveur:

Pour lancer le serveur, on utilise un docker container créé à partir de l'image du projet.  
Il est ici nécessaire d'avoir docker d'installé.
Commencez par créer l'image du projet:
```
git clone https://gitlab.com/lpbb/qpuc.git
cd qpuc/server
docker build -t image-qpuc .
```

Pour utiliser l'application, il est nécessaire d'avoir une base de données QPUC_DB.db.  
Si besoin, pour pouvoir tester l'application, il est possible de créer une base de données remplie avec quelques   
questions et joueurs pour tester.

Pour lancer l'application il suffit d'instancier un docker container:
```
docker run -p 8000:8000 -v absolute_path_to_QPUC_DB.db:/app/QPUC_DB/db image-qpuc

```

L'application est ensuite disponible sur le port `http://0.0.0.0:8000/`.

## Client

Le client est encore en cours de développement. En attendant, vous pouvez utiliser l'application via Insomnia en faisant les requêtes HTTP adapté au serveur lancé sur l'image docker. Voici une liste des requêtes possible :

- Vérifier que le serveur est bien lancé : GET('http://127.0.0.1:8000/')

- Ajouter un joueur : POST('http://127.0.0.1:8000/player/add') avec un body json sous la forme [{'id':1, 'name':'Charlie }, {'id':2, 'name':'Paul}] (Attention: l'id du joueur dans la database ne sera pas forcément le même, celui ci n'est qu'à titre informatif pour crée l'objet Player)

- Ajouter une question : POST('http://127.0.0.1:8000/question/add') avec un body json sous la forme [{'id':1, 'content':'content1', 'answer':'answer1, 'theme':'theme1', difficulty: int de 1 à 4}, {'id':2, 'content':'content2', 'answer':'answer2, 'theme':'theme2', difficulty: int de 1 à 4}] (Attention: l'id de la question dans la database ne sera pas forcément le même, celui ci n'est qu'à titre informatif pour crée l'objet Question)

- Créer une nouvelle partie : POST('http://127.0.0.1:8000/create/') avec un body json sous la forme [1,2,3] (la liste des id des joueurs de la partie)

- Passer au round suivant : POST('http://127.0.0.1:8000/play/') avec un body json sous la forme [{ "player_id" : 1, "answer": true}, { "player_id":2, "answer":false},{ "player_id" : 3, "answer": true}]

## Auteurs
Louis PERRAUD

Badr-Eddine KEBRIT

