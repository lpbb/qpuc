from database.database import Database
from models.game import Game
from models.game_factory import GameFactory
from models import *
from database.query.save_new_player import SaveNewPlayer
from database.query.save_new_question import SaveNewQuestion
from database.query.save_game_result import SaveGameResult


db=Database('QPUC_DB.db')
db.create_tables()

def setUp_database(database):
        questions = [
        ( "What is the capital of France?", "Paris", "Geography", 1),
        ( "What is the largest ocean?", "Pacific", "Geography", 2),
        ( "What is the smallest planet in our solar system?", "Mercury", "Science", 1),
        ( "What is the formula for water?", "H2O", "Science", 2),
        ( "What is the highest mountain in the world?", "Everest", "Geography", 3),
        ( "What is the largest continent by land area?", "Asia", "Geography", 2),
        ( "What is the symbol for gold?", "Au", "Science", 1),
        ( "What is the fastest land animal?", "Cheetah", "Nature", 3),
        ( "What is the largest country in the world?", "Russia", "Geography", 4),
        ( "What is the capital of Japan?", "Tokyo", "Geography", 4),
        ( "What is the fastest sea animal?", "sailfish", "Science", 4),
        ( "What is the color of the sky?", "blue", "Nature", 4),
        ( "What is the largest state in the United States by land area?", "Alaska", "Geography", 2),
        ( "What is the world s second largest ocean?", "Atlantic", "Geography", 2),
        ( "What is the smallest country in the world by land area?", "Vatican City", "Geography", 3),
        ( "What is the process by which plants convert light energy into chemical energy?", "Photosynthesis", "Science", 3),
        ( "What is the world s largest desert?", "Sahara", "Geography", 2),
        ( "What is the largest organ in the human body?", "Skin", "Science", 3),
        ( "What is the largest living organism on Earth?", "Great Barrier Reef", "Nature", 4),
        ( "What is the smallest bird in the world?", "Bee Hummingbird", "Nature", 3),
        ( "What is the tallest mammal in the world?", "Giraffe", "Nature", 2),
        ( "What is the name of the closest star to our Sun?", "Proxima Centauri", "Science", 4),
        ( "What is the largest waterfall in the world?", "Angel Falls", "Geography", 3),
        ( "What is the largest animal on Earth?", "Blue Whale", "Nature", 3),
        ( "What is the highest IQ ever recorded?", "228", "Science", 4),
        ( "What is the tallest mountain in North America?", "Denali/Mount McKinley", "Geography", 3),
        ( "What is the name of the first man to walk on the Moon?", "Neil Armstrong", "History", 4),
        ( "What is the largest city in the world by population?", "Tokyo", "Geography", 4),
        ( "What is the highest waterfall in North America?", "Yosemite Falls", "Geography", 3),
        ( "What is the name of the first artificial satellite launched into space?", "Sputnik 1", "History", 4),
        ( "What is the fastest land mammal in the world?", "Cheetah", "Nature", 4)
    ]
        for content,answer,theme,difficulty in questions:
            SaveNewQuestion(database).save(content,answer,theme,difficulty)
        player_name = ['louis','badr','cail','hugo','guillaume']
        for player in player_name:
            SaveNewPlayer(database).save(player)
        player_ids = [1,2,3,4,5]
        SaveGameResult(database).save(player_ids, [2,3,4], 3)

setUp_database(db)
