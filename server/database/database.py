import sqlite3
from abc import ABC

class Database:
    def __init__(self, db_name):
        self.db_name = db_name
        self.conn = sqlite3.connect(self.db_name)
        self.cursor = self.conn.cursor()
    
    def create_tables(self):
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS question (
                id INTEGER PRIMARY KEY,
                content TEXT,
                answer TEXT,
                theme TEXT,
                difficulty INTEGER
            );
        """)
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS player (
                id INTEGER PRIMARY KEY,
                name TEXT
            );
        """)
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS game (
                id INTEGER PRIMARY KEY,
                result INTEGER,
                FOREIGN KEY(result) REFERENCES player(id)
            );
        """)
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS played_in (
                game_id INTEGER,
                player_id INTEGER,
                PRIMARY KEY (game_id, player_id),
                FOREIGN KEY(game_id) REFERENCES game(id),
                FOREIGN KEY(player_id) REFERENCES player(id)
            )
        """)
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS belongs_to (
                game_id INTEGER,
                question_id INTEGER,
                PRIMARY KEY (game_id, question_id),
                FOREIGN KEY(game_id) REFERENCES game(id),
                FOREIGN KEY(question_id) REFERENCES question(id)
            )
        """)
        self.conn.commit()
    
    def execute_request(self, query):
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()
        cursor.execute(query)
        return cursor.fetchall()

    def execute_save(self, query):
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()

    def close(self):
        self.conn.close()





