import unittest
import sqlite3
import os
from database.query.save_game_result import SaveGameResult
from database.database import Database

class TestSaveGameResult(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()
    
    def test_save(self):
        # Check if execute_request method returns the expected result
        player_ids = [1,2,3,4,5]
        question_ids = [6,7,8,9,10]
        winner_id = 5
        SaveGameResult(self.database).save(player_ids, question_ids, winner_id)
        # Ckeck for the game table
        query_game = "SELECT * FROM game;"
        result = self.database.execute_request(query_game)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], 5)
        # Ckeck for the played_in table
        query_played_in = "SELECT * FROM played_in;"
        result = self.database.execute_request(query_played_in)
        self.assertEqual(len(result), 5)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], 1)
        self.assertEqual(result[1][0], 1)
        self.assertEqual(result[1][1], 2)
        # Ckeck for the belongs_to table
        query_belongs_to = "SELECT * FROM belongs_to;"
        result = self.database.execute_request(query_belongs_to)
        self.assertEqual(len(result), 5)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], 6)
        self.assertEqual(result[1][0], 1)
        self.assertEqual(result[1][1], 7)
    
    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)
