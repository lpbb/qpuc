import unittest
import sqlite3
import os
from database.query.save_new_player import SaveNewPlayer
from database.database import Database

class TestSaveNewPlayer(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()
    
    
    def test_save(self):
        # Check if execute_request method returns the expected result
        SaveNewPlayer(self.database).save("Alice")
        query = "SELECT * FROM player;"
        result = self.database.execute_request(query)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], "Alice")
    
    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)