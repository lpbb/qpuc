import unittest
import sqlite3
import os
from database.query.request_player_name import RequestPlayerName
from database.database import Database

class TestRequestPlayerName(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()
        query = "INSERT INTO player (name) VALUES ('Alice')"
        self.database.execute_save(query)
    
    
    def test_request(self):
        # Check if execute_request method returns the expected result
        result = RequestPlayerName(self.database).execute(1)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], 'Alice') 
    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)