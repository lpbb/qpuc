import os
import unittest
import sqlite3

from database.query.request_bonus_question import RequestBonusQuestion
from database.database import Database
from database.query.save_new_question import SaveNewQuestion
from database.query.save_new_player import SaveNewPlayer
from database.query.save_game_result import SaveGameResult


class TestRequestBonusQuestion(unittest.TestCase):

    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()
        self.setUp_database()

    def setUp_database(self):
        questions = [
            ( "What is the capital of France?", "Paris", "Geography", 1),
            ( "What is the largest ocean?", "Pacific", "Geography", 2),
            ( "What is the smallest planet in our solar system?", "Mercury", "Science", 1),
            ( "What is the formula for water?", "H2O", "Science", 2),
            ( "What is the highest mountain in the world?", "Everest", "Geography", 3),
            ( "What is the largest continent by land area?", "Asia", "Geography", 2),
            ( "What is the symbol for gold?", "Au", "Science", 1),
            ( "What is the fastest land animal?", "Cheetah", "Nature", 3),
            ( "What is the largest country in the world?", "Russia", "Geography", 4),
            ( "What is the capital of Japan?", "Tokyo", "Geography", 4),
            ( "What is the fastest see animal?", "sailfish", "Science", 4),
            ( "What is the color of the sky?", "blue", "Nature", 4)
        ]
        for content,answer,theme,difficulty in questions:
            SaveNewQuestion(self.database).save(content,answer,theme,difficulty)
        player_name = ['louis','badr','cail','hugo','guillaume']
        for player in player_name:
            SaveNewPlayer(self.database).save(player)
        player_ids = [1,2,3,4,5]
        SaveGameResult(self.database).save(player_ids, [2,3,4], 3)
        

    def test_request_list_question(self):
        player_ids = [1, 2, 3]
        expected_results_list = [(10, "What is the capital of Japan?", "Tokyo", "Geography", 4),
                                      (12, 'What is the color of the sky?', 'blue', 'Nature', 4),
                                      (11, 'What is the fastest see animal?', 'sailfish', 'Science', 4),
                                      (9, "What is the largest country in the world?", "Russia", "Geography", 4)]
        results = RequestBonusQuestion(self.database).execute(player_ids)
        self.assertIn( results, expected_results_list)
        results_next = RequestBonusQuestion(self.database).execute(player_ids,[9])
        self.assertIn(results_next, expected_results_list)
        self.assertNotEqual(results_next,(9, "What is the largest country in the world?", "Russia", "Geography", 4))


    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)
