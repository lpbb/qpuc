import unittest
import sqlite3
import os
from database.query.save_new_question import SaveNewQuestion
from database.database import Database

class TestSaveNewQuestion(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()
    
    
    def test_save(self):
        # Check if execute_request method returns the expected result
        SaveNewQuestion(self.database).save("le test marche ?", "OUI", "informatique", 1)
        query = "SELECT id, content, answer, theme, difficulty FROM question;"
        result = self.database.execute_request(query)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], "le test marche ?")
        self.assertEqual(result[0][2], "OUI")
        self.assertEqual(result[0][3], "informatique")
        self.assertEqual(result[0][4], 1)
    
    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)