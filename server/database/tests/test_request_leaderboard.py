import unittest
import sqlite3
import os
from database.query.request_leaderboard import RequestLeaderboard
from database.database import Database
from database.query.save_game_result import SaveGameResult
from database.query.save_new_player import SaveNewPlayer

class TestRequestLeaderboard(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()
        player_name = ['louis','badr','cail','hugo','guillaume']
        for player in player_name:
            SaveNewPlayer(self.database).save(player)
        player_ids = [1,2,3,4,5]
        SaveGameResult(self.database).save(player_ids, [6,7,8,9,10], 3)
        SaveGameResult(self.database).save(player_ids, [11,12,13,14,15], 3)
        SaveGameResult(self.database).save(player_ids, [16,17,18,19,20], 4)
        SaveGameResult(self.database).save(player_ids, [21,22,23,24,25], 1)
    
    
    def test_request(self):
        # Check if execute_request method returns the expected result
        result = RequestLeaderboard(self.database).execute()
        self.assertEqual(len(result), 3)
        self.assertEqual(result[0][0], 'cail')
        self.assertEqual(result[0][1], 2)
        self.assertEqual(result[1][0], 'hugo')
        self.assertEqual(result[1][1], 1)
    
    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)