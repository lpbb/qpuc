import unittest
import sqlite3
from database.tests.test_database import TestDatabase
from database.tests.test_save_game_result import TestSaveGameResult
from database.tests.test_save_new_question import TestSaveNewQuestion
from database.tests.test_request_player_exist import TestRequestPlayerExist
from database.tests.test_save_new_player import TestSaveNewPlayer
from database.tests.test_request_leaderboard import TestRequestLeaderboard
from database.tests.test_request_list_question import TestRequestListQuestion
from database.tests.test_request_bonus_question import TestRequestBonusQuestion
from database.tests.test_request_player_name import TestRequestPlayerName


if __name__ == '__main__':
    unittest.main()
