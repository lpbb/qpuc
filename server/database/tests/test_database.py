import unittest
import sqlite3
import os
from database.database import Database

class TestDatabase(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()
    
    def test_create_tables(self):
        # Check if the tables have been created
        query = "SELECT name from sqlite_master where type='table';"
        tables = [table[0] for table in self.database.execute_request(query)]
        self.assertIn('question', tables)
        self.assertIn('player', tables)
        self.assertIn('game', tables)
        self.assertIn('played_in', tables)
        self.assertIn('belongs_to', tables)
    
    def test_execute_save_request(self):
        # Check if execute_request method returns the expected result
        query = "INSERT INTO player (name) VALUES ('John Doe');"
        self.database.execute_save(query)
        query = "SELECT * FROM player;"
        result = self.database.execute_request(query)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], 'John Doe')
    
    
    
    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)
