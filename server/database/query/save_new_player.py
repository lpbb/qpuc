from database.query.make_save import SaveRequest


class SaveNewPlayer(SaveRequest):
    '''
    Cette classe permet d'ajouter un nouveau joueur à la base de donnée.
    '''
    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db

      
    def save(self, name):
        '''
        Executer la requête SQL pour sauvegarder les données du joueur dans la base de données.

        Paramètres
        ----------
        name: str, nom du joueur
        '''
        query = f"""
            INSERT INTO player (name) 
            VALUES ('{name}')
        """
        self.game_db.execute_save(query)