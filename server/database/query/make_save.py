from abc import ABC, abstractmethod


class SaveRequest(ABC):
    '''
    Cette classe est la classe mère pour les requêtes de sauvegarde de données.
    '''
    @abstractmethod
    def save(self):
        pass