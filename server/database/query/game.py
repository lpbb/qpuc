from database.query.make_request import MakeRequest


class GetGamesRequest(MakeRequest):
    def __init__(self, game_db):
        self.game_db = game_db
    
    def execute(self):
        return self.game_db.execute_query("SELECT * FROM game")