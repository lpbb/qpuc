from database.query.make_request import MakeRequest


class RequestPlayerExist(MakeRequest):
    '''
    Cette classe permet de vérifier si un joueur existe ou pas dans la base de donnée.
    '''
    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db
      
    def execute(self, player_id):
        '''
        Exectue la requête SQl pour vérifier si un joueur existe selon son id.

        Paramètres
        ----------
        player_id : int, id du joueur
        '''
        query = f"""
            SELECT COUNT(*)
            FROM player
            WHERE id = ('{player_id}')
        """
        return self.game_db.execute_request(query)