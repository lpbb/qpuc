
from database.query.make_request import MakeRequest

class GetBelongsToRequest(MakeRequest):
    '''
    Cette classe permet de récupérer les données de la table BelongsTo.
    '''
    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de donnée du projet
        '''
        self.game_db = game_db
    
    def execute(self):
        '''
        Cette méthode execute la requête SQL pour récupérer toutes les données de la table
        '''
        return self.game_db.execute_query("SELECT * FROM played_in")
