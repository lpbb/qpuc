from database.query.make_request import MakeRequest


class RequestListQuestion(MakeRequest):
    '''
    Cette classe permet d'obtenir une liste de question afin d'initer une partie.
    '''
    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db
 
    def execute(self, player_ids, difficulty):
        '''
        Exectue la requête SQl pour requêter une liste de question selon une difficulté donnée de telle sorte à ce qu'aucun
        joueur n'ai déjà rencontré une question de la liste.

        Paramètres
        ----------
        player_ids : list[int], liste des id des joueurs
        difficulty : int, difficulté désiré pour les questions
        '''
        player_ids_str = ', '.join(str(player_id) for player_id in player_ids)
        query = f"""
            SELECT id, content, answer, theme, difficulty
            FROM question
            WHERE difficulty = {difficulty}
            AND theme IN (
                SELECT DISTINCT theme FROM question WHERE difficulty = {difficulty}
            )
            AND id NOT IN (
                SELECT b.question_id
                FROM belongs_to b
                JOIN played_in p ON b.game_id = p.game_id
                WHERE p.player_id IN ({player_ids_str})
            )
            GROUP BY theme
            ORDER BY theme
            LIMIT {len(set(self.game_db.execute_request(f"SELECT theme FROM question WHERE difficulty = {difficulty}")))}
        """
        return self.game_db.execute_request(query)
