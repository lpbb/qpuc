from database.query.make_request import MakeRequest


class RequestLeaderboard(MakeRequest):
    '''
    Cette classe permet d'obtenir les noms des joueurs de la base de donnée et leur nombre de victoire.
    '''
    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db
      
    def execute(self):
        '''
        Exectue la requête SQl pour avoir le nombre de victoire des joueurs de la base de données.
        '''
        query = f"""
            SELECT player.name, COUNT(*) AS wins 
            FROM game 
            JOIN player ON game.result = player.id 
            GROUP BY player.id 
            ORDER BY wins DESC 
            LIMIT 10
        """
        return self.game_db.execute_request(query)