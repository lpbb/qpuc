from database.query.make_save import SaveRequest


class SaveNewQuestion(SaveRequest):
    '''
    Cette classe permet d'ajouter une nouvelle question à la base de donnée.
    '''

    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db

    def save(self, content, answer, theme, difficulty):
        '''
        Exectue la requête permettant de sauvegarder une nouvelle question dans la base de données.

        Paramètres
        ----------
        content : str, contenue de la question
        answer : str, réponse de la question
        theme : str, thème de la question
        difficulty : int, difficulté de la question
        '''
        query = f"""
            INSERT INTO question (content, answer, theme, difficulty) 
            VALUES ('{content}', '{answer}', '{theme}', '{difficulty}')
        """
        self.game_db.execute_save(query)
