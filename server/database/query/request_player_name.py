from database.query.make_request import MakeRequest


class RequestPlayerName(MakeRequest):
    '''
    Cette classe permet de requêter le nom d'un joueur de la base de donnée à partir de son id.
    '''
    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db
      
    def execute(self, player_id):
        '''
        Exectue la requête SQl pour avoir les donées du joueur selon son id.

        Paramètres
        ----------
        player_id : int, id du joueur
        '''
        query = f"""
            SELECT id, name
            FROM player
            WHERE id = ('{player_id}')
        """
        return self.game_db.execute_request(query)