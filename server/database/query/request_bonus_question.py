from database.query.make_request import MakeRequest


class RequestBonusQuestion(MakeRequest):
    '''
    Cette classe permet d'obtenir une question bonus lorsqu'il y en a plus dans la partie.
    '''

    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db

    def execute(self, player_ids, question_ids=[]):
        '''
        Exectue la requête SQl pour avoir une question bonus qui n'a jamais été posé à aucun joueur.

        Paramètres
        ----------
        player_ids : list[int], liste des id des joueurs
        question_ids : list[question], liste des id des questions
        '''
        player_ids_str = ', '.join(str(player_id) for player_id in player_ids)
        question_ids_str = ', '.join(str(question_id)
                                     for question_id in question_ids)
        query = f"""
            SELECT q.id, q.content, q.answer, q.theme, q.difficulty
            FROM question q
            WHERE q.id NOT IN (
                SELECT b.question_id
                FROM belongs_to b
                JOIN played_in p ON b.game_id = p.game_id
                WHERE p.player_id IN ({player_ids_str})
            ) AND q.difficulty == 4 AND q.id NOT IN ({question_ids_str})
            ORDER BY RANDOM()
            LIMIT 1
        """
        return self.game_db.execute_request(query)[0]
