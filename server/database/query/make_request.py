from abc import ABC, abstractmethod

class MakeRequest(ABC):
    '''
    Cette classe est la classe mère pour les requêtes de selection.
    '''
    @abstractmethod
    def execute(self):
        pass


