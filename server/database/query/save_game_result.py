from database.query.make_save import SaveRequest
import sqlite3

class SaveGameResult(SaveRequest):
    '''
    Cette classe permet de sauvegarder les résultats d'une partie.
    '''
    def __init__(self, game_db):
        '''
        Arguments
        -------
        game_db : Database, base de données du projets
        '''
        self.game_db = game_db

    def save(self, player_ids, question_ids, winner_id):
        '''
        Execute la requête SQL pour sauvegarder les informations de la partie dans la base de données.

        Paramètres
        ----------
        player_ids : list[int], liste des id des joueurs
        questions_ids : list[int], liste des id des questions
        winner_id : int, id du joueur gagnant
        '''
        # Add the new game to the "game" table
        query = f"INSERT INTO game (result) VALUES ('{winner_id}')"
        self.game_db.execute_save(query)
        game_id = self.game_db.execute_request("SELECT game.id FROM game ORDER BY id DESC")[0][0]
       # Add the players who played in the game to the "played_in" table
        for player_id in player_ids:
            query = f"INSERT INTO played_in (game_id, player_id) VALUES ('{game_id}', '{player_id}');"
            self.game_db.execute_save(query)
        # Add the questions of the game to the "belongs_to" table
        for question_id in question_ids:
            query = f"INSERT INTO belongs_to (game_id, question_id) VALUES ('{game_id}', '{question_id}');"
            self.game_db.execute_save(query)