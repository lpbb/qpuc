import sqlite3

# Connect to the database
conn = sqlite3.connect('DBtest.db')

# Create a cursor
cursor = conn.cursor()

# Insert sample questions into the question table
# cursor.execute("INSERT INTO question (content, difficulty, theme, answer) VALUES ('What is the capital of germany?', 1, 'Geography', 'Berlin')")
# cursor.execute("INSERT INTO question (content, difficulty, theme, answer) VALUES ('What is the fastest see animal?', 2, 'Animals', 'ork')")
# cursor.execute("INSERT INTO question (content, difficulty, theme, answer) VALUES ('What is the second highest mountain in the world?', 3, 'Geography', 'K2')")

cursor.execute("INSERT INTO question (content, difficulty, theme, answer) VALUES ('What is the thrid highest mountain in the world?', 4, 'Geography', 'ma bite')")


# # Insert sample players into the player table
# cursor.execute("INSERT INTO player (name) VALUES ('Alice')")
# cursor.execute("INSERT INTO player (name) VALUES ('Bob')")
# cursor.execute("INSERT INTO player (name) VALUES ('Charlie')")

# # Insert sample games into the game table
# cursor.execute("INSERT INTO game (result) VALUES (10)")
# cursor.execute("INSERT INTO game (result) VALUES (5)")
# cursor.execute("INSERT INTO game (result) VALUES (0)")

# # Insert sample played_in into the game table
# cursor.execute("INSERT INTO played_in (player_id, game_id) VALUES (1,1)")
# cursor.execute("INSERT INTO played_in (player_id, game_id) VALUES (2,1)")
# cursor.execute("INSERT INTO played_in (player_id, game_id) VALUES (3,1)")

# # Insert sample belongs_to into the game table
# cursor.execute("INSERT INTO belongs_to (question_id, game_id) VALUES (1,1)")
# cursor.execute("INSERT INTO belongs_to (question_id, game_id) VALUES (2,1)")
# cursor.execute("INSERT INTO belongs_to (question_id, game_id) VALUES (3,1)")
# cursor.execute("INSERT INTO belongs_to (question_id, game_id) VALUES (10,1)")
# Commit the changes and close the connection
conn.commit()
conn.close()
