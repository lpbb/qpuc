from models.factory import Factory
from models.player import Player


class PlayerFactory(Factory):
    '''
    Cette classe permet de créer des objets de la classe Player.
    '''
    def create(self, list_player) -> list[Player]:
        '''
        Cette méthode retourne des objets Player, il est donc possible de créer plusieurs joueur en un seul appel 
        de cette fonction.

        Paramètres
        ----------
        list_player : list[list] , une liste de liste contenant les informations des joueurs à créer

        Returns
        -------
        list_new_player : list[Player], une liste des objets Player crées
        '''
        list_new_player = []
        for player in list_player:
            new_player = Player(player[0], player[1])
            list_new_player.append(new_player)
        return list_new_player
