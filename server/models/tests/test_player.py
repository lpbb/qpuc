import sqlite3
import os
import unittest
from models.player import Player
from database.database import Database

class TestPlayer(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()

    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)

    def test_change_life(self):
        player = Player(1, 'John')
        player.change_life(-2)
        self.assertEqual(player.nb_life, 3)
        self.assertEqual(player.is_alive, True)

        player.change_life(-3)
        self.assertEqual(player.nb_life, 0)
        self.assertEqual(player.is_alive, False)

    def test_increment_score(self):
        player = Player(1, 'John')
        player.increment_score()
        self.assertEqual(player.score, 1)

        player.increment_score()
        self.assertEqual(player.score, 2)

    def test_save(self):
        player = Player(1, 'John')
        player.save(self.database)
        query = "SELECT * FROM player;"
        result = self.database.execute_request(query)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], "John")

    def test_dict(self):
        player = Player(1, 'John')
        expected_dict = {
            'player id': 1,
            'life': 5,
            'score': 0,
            'name': 'John',
            'is_alive': True
        }
        self.assertEqual(player.dict(), expected_dict)
