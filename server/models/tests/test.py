import unittest

from models.tests.test_player import TestPlayer
from models.tests.test_question import TestQuestion
from models.tests.test_game import TestGame


from models.tests.test_player_factory import TestPlayerFactory
from models.tests.test_question_factory import TestQuestionFactory


if __name__ == '__main__':
    unittest.main()
