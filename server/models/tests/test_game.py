import unittest
import os
from models.player import Player
from models.question import Question
from database.query.save_game_result import SaveGameResult
from database.query.request_bonus_question import RequestBonusQuestion
from models.question_factory import QuestionFactory
from database.database import Database
from models.game_factory import GameFactory
from database.query.save_new_player import SaveNewPlayer
from database.query.save_new_question import SaveNewQuestion


class TestGame(unittest.TestCase):
       
    def setUp(self):
        self.database = Database('test.db')
        self.database.create_tables()
        questions = [
            ( "What is the capital of France?", "Paris", "Geography", 1),
            ( "What is the largest ocean?", "Pacific", "Geography", 2),
            ( "What is the smallest planet in our solar system?", "Mercury", "Science", 1),
            ( "What is the formula for water?", "H2O", "Science", 2),
            ( "What is the highest mountain in the world?", "Everest", "Geography", 3),
            ( "What is the largest continent by land area?", "Asia", "Geography", 2),
            ( "What is the symbol for gold?", "Au", "Science", 1),
            ( "What is the fastest land animal?", "Cheetah", "Nature", 3),
            ( "What is the largest country in the world?", "Russia", "Geography", 4),
            ( "What is the capital of Japan?", "Tokyo", "Geography", 4),
            ( "What is the fastest see animal?", "sailfish", "Science", 4),
            ( "What is the color of the sky?", "blue", "Nature", 4)
        ]
        for content,answer,theme,difficulty in questions:
            SaveNewQuestion(self.database).save(content,answer,theme,difficulty)
        player_name = ['louis','badr','cail','hugo','guillaume']
        for player in player_name:
            SaveNewPlayer(self.database).save(player)
        player_ids = [1,2,3,4,5]
        SaveGameResult(self.database).save(player_ids, [2,3,4], 4)
        self.game = GameFactory().create(player_ids,self.database)

    def tearDown(self):
        self.database.close()
        os.remove('test.db')

    def test_start_game(self):
        initiate =self.game.start_game()
        is_over = initiate['is_over']
        first_question_dict= initiate['next question'].dict()
        list_player_dict = [player.dict() for player in initiate['players']]
        first_question_dict_expected = {'content': 'What is the capital of France?', 'answer': 'Paris', 'theme': 'Geography', 'difficulty': 1}
        list_player_dict_expected =[{'player id': 1, 'life': 5, 'score': 0, 'name': 'louis', 'is_alive': True},
                                     {'player id': 2, 'life': 5, 'score': 0, 'name': 'badr', 'is_alive': True},
                                       {'player id': 3, 'life': 5, 'score': 0, 'name': 'cail', 'is_alive': True},
                                         {'player id': 4, 'life': 5, 'score': 0, 'name': 'hugo', 'is_alive': True},
                                           {'player id': 5, 'life': 5, 'score': 0, 'name': 'guillaume', 'is_alive': True}]
        self.assertEqual(first_question_dict,first_question_dict_expected)
        self.assertEqual(list_player_dict,list_player_dict_expected)
        self.assertFalse(is_over)

    def test_get_player_with_id(self):
        player_dict = self.game.get_player_with_id(1).dict()
        player_dict_expected = {'player id': 1, 'life': 5, 'score': 0, 'name': 'louis', 'is_alive': True}
        self.assertEqual(player_dict,player_dict_expected)

    def test_update_players_life(self):
        list_answer = [{'player_id': 1, 'answer': False},
                       {'player_id': 2, 'answer': False},
                       {'player_id': 3, 'answer': True},
                       {'player_id': 4, 'answer': True},
                       {'player_id': 5, 'answer': False}]
        self.game.update_players_life(list_answer)
        self.assertEqual(self.game.list_player[0].score, 0)
        self.assertEqual(self.game.list_player[1].nb_life, 4)
        self.assertEqual(self.game.list_player[2].nb_life, 5)
        self.assertEqual(self.game.list_player[2].score, 1)
        self.assertEqual(self.game.list_player[2].is_alive, True)
        for i in range(4):
            self.game.update_players_life(list_answer)
        self.assertEqual(self.game.list_player[0].score, 0)
        self.assertEqual(self.game.list_player[1].nb_life, 0)
        self.assertEqual(self.game.list_player[2].nb_life, 5)
        self.assertEqual(self.game.list_player[2].score, 5)
        self.assertEqual(self.game.list_player[2].is_alive, True)
        self.assertEqual(self.game.list_player[0].is_alive, False)



    def test_next_question(self):
        next_question_dict = self.game.next_question().dict()
        next_question_dict_expected = {'content': 'What is the symbol for gold?', 'answer': 'Au', 'theme': 'Science', 'difficulty': 1}
        self.assertEqual(next_question_dict,next_question_dict_expected)
        for i in range(len(self.game.list_question)-2):
            self.game.next_question().dict()
        next_question_bonus = self.game.next_question()
        self.assertIsInstance(next_question_bonus, Question)

    def test_get_winner_id(self):
        list_answer = [{'player_id': 1, 'answer': False},
                       {'player_id': 2, 'answer': False},
                       {'player_id': 3, 'answer': True},
                       {'player_id': 4, 'answer': False},
                       {'player_id': 5, 'answer': False}]
        for i in range(5):
            self.game.update_players_life(list_answer)
        winner_id = self.game.get_winner_id()
        winner_id_expected = 3
        self.assertEqual(winner_id,winner_id_expected)
    
    def test_save(self):
        list_answer = [{'player_id': 1, 'answer': False},
                       {'player_id': 2, 'answer': False},
                       {'player_id': 3, 'answer': True},
                       {'player_id': 4, 'answer': False},
                       {'player_id': 5, 'answer': False}]
        for i in range(5):
            self.game.play(list_answer)
        self.assertEqual(self.game.is_over, True)
        self.game.save()
        query = self.database.execute_request('SELECT * FROM game')[1][1]
        query_expected =3
        self.assertEqual(query,query_expected)



    def test_play(self):
        list_answer = [{'player_id': 1, 'answer': False},
                       {'player_id': 2, 'answer': False},
                       {'player_id': 3, 'answer': True},
                       {'player_id': 4, 'answer': False},
                       {'player_id': 5, 'answer': False}]

        play_result =self.game.play(list_answer)
        next_question_dict= play_result['next question'].dict()
        list_player_dict = [player.dict() for player in play_result['players']]
        next_question_dict_expected = {'content': 'What is the symbol for gold?', 'answer': 'Au', 'theme': 'Science', 'difficulty': 1}
        list_player_dict_expected =[{'player id': 1, 'life': 4, 'score': 0, 'name': 'louis', 'is_alive': True},
                                     {'player id': 2, 'life': 4, 'score': 0, 'name': 'badr', 'is_alive': True},
                                       {'player id': 3, 'life': 5, 'score': 1, 'name': 'cail', 'is_alive': True},
                                         {'player id': 4, 'life': 4, 'score': 0, 'name': 'hugo', 'is_alive': True},
                                           {'player id': 5, 'life': 4, 'score': 0, 'name': 'guillaume', 'is_alive': True}]
        self.assertEqual(next_question_dict,next_question_dict_expected)
        self.assertEqual(list_player_dict,list_player_dict_expected)

