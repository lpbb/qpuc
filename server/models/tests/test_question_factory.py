import unittest
from models.question import Question
from models.question_factory import QuestionFactory


class TestQuestionFactory(unittest.TestCase):
    def setUp(self):
        self.factory = QuestionFactory()

    def test_create(self):
        list_question = [
            (1, 'What is the capital of France?', 'Paris', 'Geography', 2),
            (2, 'Who is the author of "The Catcher in the Rye"?', 'J.D. Salinger', 'Literature', 3),
            (3, 'What is the formula for water?', 'H2O', 'Chemistry', 1)
        ]
        expected_result = [
            Question(1, 'What is the capital of France?', 'Paris', 'Geography', 2),
            Question(2, 'Who is the author of "The Catcher in the Rye"?', 'J.D. Salinger', 'Literature', 3),
            Question(3, 'What is the formula for water?', 'H2O', 'Chemistry', 1)
        ]
        result = self.factory.create(list_question)
        self.assertEqual(len(result), 3)
        for i in range(len(result)):
            self.assertEqual(result[i].id, expected_result[i].id)
            self.assertEqual(result[i].content, expected_result[i].content)
            self.assertEqual(result[i].answer, expected_result[i].answer)
            self.assertEqual(result[i].theme, expected_result[i].theme)
            self.assertEqual(result[i].difficulty, expected_result[i].difficulty)

