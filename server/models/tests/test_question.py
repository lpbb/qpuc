import sqlite3
import os
import unittest
from models.question import Question
from database.database import Database

class TestQuestion(unittest.TestCase):
    def setUp(self):
        self.db_name = "test_database.db"
        self.database = Database(self.db_name)
        self.database.create_tables()

    def tearDown(self):
        self.database.close()
        os.remove(self.db_name)

    def test_save(self):
        question = Question(1, 'What is the capital of France?', 'Paris', 'Geography', 1)
        question.save(self.database)
        query = "SELECT * FROM question;"
        result = self.database.execute_request(query)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0][0], 1)
        self.assertEqual(result[0][1], 'What is the capital of France?')
        self.assertEqual(result[0][2], 'Paris')
        self.assertEqual(result[0][3], 'Geography')
        self.assertEqual(result[0][4], 1)

    def test_dict(self):
        question = Question(1, 'What is the capital of France?', 'Paris', 'Geography', 1)
        expected_dict = {
            'content': 'What is the capital of France?',
            'answer': 'Paris',
            'theme': 'Geography',
            'difficulty': 1
        }
        self.assertEqual(question.dict(), expected_dict)
