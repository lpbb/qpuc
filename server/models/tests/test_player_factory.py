import unittest
from models.player import Player
from models.player_factory import PlayerFactory


class TestPlayerFactory(unittest.TestCase):
    def setUp(self):
        self.factory = PlayerFactory()

    def test_create(self):
        list_player = [(1, 'John'), (2, 'Jane'), (3, 'Bob')]
        expected_result = [
            Player(1, 'John'),
            Player(2, 'Jane'),
            Player(3, 'Bob')
        ]
        result = self.factory.create(list_player)
        self.assertEqual(len(result), 3)
        for i in range(len(result)):
            self.assertEqual(result[i].id, expected_result[i].id)
            self.assertEqual(result[i].name, expected_result[i].name)

