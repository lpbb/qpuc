from database.query.save_new_question import SaveNewQuestion


class Question():
    '''
    Cette classe caractérise les questions d'une partie.
    '''

    def __init__(self, id, content, answer, theme, difficulty):
        '''
        Arguments
        ----------
        id : int, id de la question
        content : str, contenue de la question
        answer : str, réponse de la question
        theme : str, theme de la question (sport, jeux vidéo, science ...)
        difficulty : int, difficulté de la question, allant de 1 à 4
        '''
        self.id = id
        self.content = content
        self.answer = answer
        self.theme = theme
        self.difficulty = difficulty

    def save(self, database):
        '''
        Sauvegarde les données de la question dans la base de données

        Paramètres
        ----------
        database : Database, base de donnée utilisé pour le projet
        '''
        savequestion = SaveNewQuestion(database)
        savequestion.save(self.content, self.answer,
                          self.theme, self.difficulty)

    def dict(self):
        '''
        Donne tout les informations de la question sous forme de dictionnaire.

        Returns
        -------
        dict : dict, dictionnaire contenant tout les arguments du joueur.
        '''
        dict = {}
        dict['content'] = self.content
        dict['answer'] = self.answer
        dict['theme'] = self.theme
        dict['difficulty'] = self.difficulty
        return dict