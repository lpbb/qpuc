from models.factory import Factory
from models.game import Game
from models.question_factory import QuestionFactory
from models.player_factory import PlayerFactory
from database.query.request_list_question import RequestListQuestion
from database.query.request_player_name import RequestPlayerName
from database.database import Database


class GameFactory(Factory):
    '''
    Cette classe permet de crée des objets de la classe Game
    '''

    def create_list_question(self, list_player_id, database):
        '''
        Créer une liste de question qui représentera les questions posées dans la nouvelle partie crée.
        Cette méthode veille bien à ce qu'aucune question déjà rencontré dans une partie précédente par l'un des joueurs
        ne soient posée une nouvelle fois lors de la nouvelle partie.

        Paramètres
        ----------
        list_player_id : list[int], liste contenant les id de chaque joueur de la partie

        database : Database, base de données où sont récupéré les questions


        Returns
        -------
        list_question : list[Question], retourne une liste d'objets Question
        '''
        request_list_question =[]
        for difficulty in [1,2,3,4]:
            request_list_question.append(RequestListQuestion(database).execute(list_player_id, difficulty))
        request_list_question = [question for sublist_difficulty in request_list_question for question in sublist_difficulty]
        list_question = QuestionFactory().create(request_list_question)
        return list_question

    def create_list_player(self,list_player_id, database):
        '''
        Cette méthode prend en entrée les id de chaque joueur et va récupérer leur informations dans la database pour crée
        les objets de la classe Player correspondant.

        Paramètres
        ----------
        list_player_id : list[int], liste contenant les id de chaque joueur de la partie

        database : Database, base de données où sont récupéré les questions


        Returns
        -------
        list_player : list[Player], retourne une liste d'objets Player
        '''
        list_player_name_id =[]
        for player_id in list_player_id:
            list_player_name_id.append(RequestPlayerName(database).execute(player_id)[0])
        list_player = PlayerFactory().create(list_player_name_id)
        return list_player

    
    def create(self, list_player_id, database):
        '''
        Cette méthode sert à créer des objets de la classe Game. Elle instancie une nouvelle game où chaque joueur n'aura jamais
        rencontré les questions posées lors d'une partie précédente.

        Paramètres
        ----------
        list_player_id : list[int], liste contenant les id de chaque joueur de la partie

        database : Database, base de données où sont récupéré les questions
        '''
        list_question = self.create_list_question(list_player_id, database)
        list_player = self.create_list_player(list_player_id, database)
        new_game = Game(database , list_player, list_question)
        return new_game
