from database.query.save_new_player import SaveNewPlayer

class Player():
    '''
    Cette classe caractérises les joueurs de la partie.
    '''
    def __init__(self, id, name):
        '''
        Arguments
        ----------
        id : int, id du joueur
        name : str, nom du joueur
        nb_life : int, nombre de vie restant au joueur
        is_alive : boolean, True si le joueur est encore en vie
        score : int, score du joueur
        '''
        self.id = id
        self.name = name
        self.nb_life = 5
        self.is_alive = True
        self.score = 0

    def change_life(self, number):
        '''
        Permet de modifier la vie d'un joueur, si sa vie tombe à zéro le joueur meurt.

        Paramètres
        ----------
        number : int, nombre de point de vie à augmenter ou enlever.
        '''
        self.nb_life += number
        if self.nb_life == 0:
            self.is_alive = False
    
    def increment_score(self):
        '''
        Augmente le score d'un joueur
        '''
        self.score += 1

    def save(self,database):
        '''
        Sauvegarde les données d'un joueur dans la base de données

        Paramètres
        ----------
        database : Database, base de donnée utilisé pour le projet
        '''
        saveplayer = SaveNewPlayer(database)
        saveplayer.save(self.name)

    def dict(self):
        '''
        Donne tout les informations du joueur sous forme de dictionnaire.

        Returns
        -------
        dict : dict, dictionnaire contenant tout les arguments du joueur.
        '''
        dict = {}
        dict['player id'] = self.id
        dict['life'] = self.nb_life
        dict['score'] = self.score
        dict['name'] = self.name
        dict['is_alive'] = self.is_alive
        return dict