from models.player import Player
from models.question import Question
from database.query.save_game_result import SaveGameResult
from database.query.request_bonus_question import RequestBonusQuestion
from models.question_factory import QuestionFactory
from models.player_factory import PlayerFactory


class Game():

    def __init__(self,database, list_player, list_question):
        self.database = database
        self.list_player = list_player
        self.list_question = list_question
        self.id_question = 0
        self.is_over = False

    def start_game(self):
        state ={
            'next question': self.list_question[0],
            'players': self.list_player,
            'is_over': self.is_over
            }
        return state

    def get_player_with_id(self, id : int) -> Player:
        player_from_id = PlayerFactory().create([(None,'')])[0]
        for player in self.list_player:
            if player.id == id:
                player_from_id = player
        return player_from_id


    def update_players_life(self, list_answer):
        if not self.is_over:
            for player_answer in list_answer:
                player = self.get_player_with_id(player_answer['player_id'])
                if player_answer['answer']:
                    player.increment_score()
                else:
                    player.change_life(-1)
            player_alive = 0
            for player in self.list_player:
                if player.is_alive:
                    player_alive += 1
            if player_alive == 0 :
                for player_answer in list_answer:
                    player = self.get_player_with_id(player_answer['player_id'])
                    player.change_life(1)
                    player.is_alive = True
            elif player_alive == 1 :
                self.is_over = True

    def next_question(self):
        next_question = QuestionFactory().create([(None,'','','', None)])
        if not self.is_over :
            if self.id_question == len(self.list_question) - 1:
                request_bonus_question = [RequestBonusQuestion(self.database).execute([player.id for player in self.list_player],[question.id for question in self.list_question])]
                bonus_question = QuestionFactory().create(request_bonus_question)[0]
                self.list_question.append(bonus_question)
            self.id_question += 1
        next_question = self.list_question[self.id_question]
        return next_question
    
    def get_winner_id(self):
        winner_id= 0
        for player in self.list_player:
            if player.nb_life > 0:
                winner_id= player.id
        return winner_id

    def save(self):
        if self.is_over:
            winner = self.get_winner_id()
            list_player_id =[player.id for player in self.list_player]
            list_question_id = [question.id for question in self.list_question]
            savegame = SaveGameResult(self.database)
            savegame.save(list_player_id, list_question_id, winner)


    def play(self,list_answer = []):
        self.update_players_life(list_answer)
        state = {
            'next question': self.next_question(),
            'players': self.list_player,
            'is_over': self.is_over
            }
        return state