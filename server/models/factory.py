from abc import ABC, abstractmethod


class Factory(ABC):
    '''
    Cette classe sert de classe mère pour le design pattern Factory
    '''

    @abstractmethod
    def create():
        pass
