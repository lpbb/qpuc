from models.factory import Factory
from models.question import Question


class QuestionFactory(Factory):
    '''
    Cette classe permet de créer des objets de la classe Question.
    '''

    def create(self, list_question) -> list[Question]:
        '''
        Cette méthode retourne des objets Question, il est donc possible de créer plusieurs questions en un seul appel 
        de cette fonction.

        Paramètres
        ----------
        list_question : list[list] , une liste de liste contenant les informations des questions à créer

        Returns
        -------
        list_new_question : list[Question], une liste des objets Question crées
        '''
        list_new_question = []
        for question in list_question:
            new_question = Question(question[0], question[1],
                                    question[2], question[3], question[4])
            list_new_question.append(new_question)
        return list_new_question

