import uvicorn
from fastapi import FastAPI
from database.database import Database
from models.game_factory import GameFactory
from models.question_factory import QuestionFactory
from models.player_factory import PlayerFactory

app = FastAPI()

db = Database('QPUC_DB.db')
db.create_tables()
Game = GameFactory().create(list_player_id= [],database =db)

@app.get("/")
def app_running():
    return {"state": 'running'}


@app.post("/player/add")
def add_player_to_db(players_dict_list: list[dict] =[]):
    global db
    players_tuple_list =[]
    for player_dict in players_dict_list:
        players_tuple_list.append(tuple(player_dict.values()))
    player_object_list= PlayerFactory().create(players_tuple_list)
    for player in player_object_list:
        player.save(db)
    return [player.dict() for player in player_object_list]


@app.post("/question/add")
def add_question_to_db(questions_dict_list: list[dict]):
    global db
    question_tuple_list =[]
    for question_dict in questions_dict_list:
        question_tuple_list.append(tuple(question_dict.values()))
    question_object_list= QuestionFactory().create(question_tuple_list)
    for question in question_object_list:
        question.save(db)
    return [question.dict() for question in question_object_list]

@app.post("/create/")
def create_game(list_player_id: list =[]):
    global Game
    Game = GameFactory().create(list_player_id,database= db)
    return Game.start_game()


@app.post("/play")
def play_round(list_answer: list[dict] =[]):
    global Game
    over_before = Game.is_over
    result = Game.play(list_answer= list_answer)
    if result['is_over'] and not over_before:
        Game.save()
    return result

if __name__ == '__main__':
    uvicorn.run("main:app", host='127.0.0.1', port=8005, log_level="info", reload=True)
    print("running")